import os
import torch
import torch.optim as optim
import torchvision.utils as vutils
from torch.autograd import Variable
from losses import Loss
from utils import Initialize_networks
import torch.nn as nn
import logging

logging.basicConfig(filename='txt_to_img.log', filemode='w',
                    format='%(asctime)s - %(message)s', level=logging.INFO)


class Model():
    """ This class defines the training and testing modules
 for the vaegan model"""

    def __init__(self, data_loader, opt):
        self.data_loader = data_loader
        self.opt = opt
        self.batch_size = opt.batchsize
        self.imgsize = opt.imgsize
        self.nz = int(opt.nz)
        self.ngf = int(opt.ngf)
        self.ndf = int(opt.ndf)
        self.nte = int(opt.nte)
        self.nt = int(opt.nt)
        self.nc = 3
        self.netG, self.netD, self.netE = Initialize_networks(self.nz, self.nc,
                                                              self.nte, self.nt,
                                                              self.ngf, self.ndf,
                                                              self.opt.netD,
                                                              self.opt.netG,
                                                              self.opt.netE)

    def train(self):
        """ This function is used for training the network."""
        print(self.opt)
        inp = torch.FloatTensor(self.batch_size, self.nc,
                                self.imgsize, self.imgsize)
        noise = torch.FloatTensor(self.batch_size, self.nz, 1, 1)
        criterion = nn.BCELoss()
        label = torch.FloatTensor(self.batch_size)
        if self.opt.cuda:
            print("cuda True")
            self.netE.cuda()
            self.netD.cuda()
            self.netG.cuda()
            inp = inp.cuda()
            noise = noise.cuda()
            criterion = criterion.cuda()
            label = label.cuda()

        optimizerD = optim.Adam(self.netD.parameters(), lr=self.opt.lr,
                                betas=(self.opt.beta1, 0.999))
        optimizerG = optim.Adam(self.netG.parameters(), lr=self.opt.lr,
                                betas=(self.opt.beta1, 0.999))
        optimizerE = optim.Adam(self.netE.parameters(), lr=self.opt.lr,
                                betas=(self.opt.beta1, 0.999))

        for epoch in range(1, self.opt.nEpochs + 1):
            for i, data in enumerate(self.data_loader, 0):
                self.real_img, self.text_embed, _ = data
                self.text_embed = Variable(self.text_embed)
                if self.opt.cuda:
                    self.real_img = self.real_img.cuda()
                    self.text_embed = self.text_embed.cuda()
                loss = Loss(self.netD, self.netG, self.netE, self.real_img,
                            self.text_embed, inp, noise, label)
                # Train Discriminator
                self.netD.zero_grad()
                errD_real, errD_wrong, errD_enc, errD_fake = loss.D_loss()
                self.errD = errD_real + errD_wrong + errD_enc + errD_fake
                logging.info("Loss_D at epoch %d is %d", epoch, self.errD)
                optimizerD.step()

                # Train Generator
                self.netG.zero_grad()
                errG_fake, errG_enc = loss.G_loss()
                self.errG = errG_fake + errG_enc
                logging.info("Loss_G at epoch %d is %d", epoch, self.errG)
                optimizerG.step()

                # Train Encoder
                self.netE.zero_grad()
                self.errE = loss.E_loss()
                logging.info("Loss_E at epoch %d is %d", epoch, self.errE)
                optimizerE.step()

                self.printlosses(epoch, i)
                if i % 100 == 0:
                    self.visualize(epoch, self.netG)
                    self.save_models(epoch, self.netD, self.netG, self.netE)

    def printlosses(self, epoch, i):
        """ This function is used to print the losses
 obtained during training."""

        print('[%d/%d][%d/%d] Loss_D: %.4f Loss_G: %.4f Loss_E: %.4f'
              % (epoch, self.opt.nEpochs, i, len(self.data_loader),
                 self.errD.data, self.errG.data, self.errE.data))

    def visualize(self, epoch, netG):
        """This function is used to visualize the image generated
 at that epoch"""

        vutils.save_image(self.real_img, '%s/real_samples.png'
                          % self.opt.outdir, normalize=True)
        fixed_noise = torch.FloatTensor(self.batch_size, self.nz, 1, 1)
        if self.opt.cuda:
            fixed_noise = fixed_noise.cuda()
        fixed_noise = Variable(fixed_noise)
        fake = netG(fixed_noise, self.text_embed)
        vutils.save_image(fake.data, '%s/fake_samples_epoch_%03d.png'
                          % (self.opt.outdir, epoch), normalize=True)
        logging.info("The image generated: %s/fake_samples_epoch_%03d",
                     self.opt.outdir, epoch)

    def save_models(self, epoch, netD, netG, netE):
        """ This function is used to save the weights of the trained models."""

        torch.save(netD.state_dict(), '%s/netD_epoch_%d.pth'
                   % (self.opt.outdir, epoch))
        torch.save(netG.state_dict(), '%s/netG_epoch_%d.pth'
                   % (self.opt.outdir, epoch))
        torch.save(netE.state_dict(), '%s/netE_epoch_%d.pth'
                   % (self.opt.outdir, epoch))

    def test(self):
        """ This function is used for testing the network."""

        noise = torch.FloatTensor(self.batch_size, self.nz, 1, 1)

        for i, data in enumerate(self.data_loader):
            real_img, text_embed, caption = data
            batch_size = real_img.size(0)
            text_embed = Variable(text_embed)

            if self.opt.cuda:
                real_img = real_img.cuda()
                text_embed = text_embed.cuda()
                noise = noise.cuda()
            noise.resize_(batch_size, self.nz, 1, 1).normal_(0, 1)
            noisev = Variable(noise)
            gen_img = self.netG(noisev, text_embed).detach()

            for i in range(gen_img.size()[0]):
                cap = caption[i].strip('.')
                cap = cap.replace("/", "or")
                cap = cap.replace(" ", "_")

                file_path = os.path.join(self.opt.eval_dir, cap, '.jpg')
                vutils.save_image(gen_img[i].data, file_path)
                logging.info("The generated image is stored at %s", file_path)
