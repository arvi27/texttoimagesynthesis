import torch
import torch.nn as nn
import torchvision.models as models


class Encoder(nn.Module):
    """This class defines an Encoder model"""

    def __init__(self, nz):
        super(Encoder, self).__init__()
        self.nz = nz

        self.resnet = models.resnet18(pretrained=True)
        self.resnet.avgpool = nn.AvgPool2d(4, 1, 0)
        self.resnet = nn.Sequential(*list(self.resnet.children())[:-2])

        self.x_to_mu = nn.Linear(2048, self.nz)
        self.x_to_logvar = nn.Linear(2048, self.nz)

    def reparameterize(self, x):
        mu = self.x_to_mu(x)
        logvar = self.x_to_logvar(x)
        z = torch.randn(mu.size())
        z = z.cuda()
        z = mu + z * torch.exp(0.5 * logvar)
        kld = (-0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp(), 1))
        return z, kld

    def forward(self, x):
        x = self.resnet(x).squeeze().view(x.size(0), -1)
        z, kld = self.reparameterize(x)
        return z, kld


class Generator(nn.Module):
    """ This class deines a generator network"""

    def __init__(self, nz, ngf, nc, nte, nt):
        super(Generator, self).__init__()
        self.nt = nt
        self.main = nn.Sequential(
            #  output z from the encoder as input
            nn.ConvTranspose2d(nz+nt, ngf*8, 4, 1, 0, bias=False),
            nn.BatchNorm2d(ngf*8),

            nn.Conv2d(ngf*8, ngf*2, 1, 1),
            nn.Dropout2d(inplace=True),
            nn.BatchNorm2d(ngf*2),
            nn.ReLU(True),


            nn.Conv2d(ngf*2, ngf*2, 3, 1, 1),
            nn.Dropout2d(inplace=True),
            nn.BatchNorm2d(ngf*2),
            nn.ReLU(True),


            nn.Conv2d(ngf*2, ngf*8, 3, 1, 1),
            nn.Dropout2d(inplace=True),
            nn.BatchNorm2d(ngf*8),
            nn.ReLU(inplace=True),


            nn.ConvTranspose2d(ngf*8, ngf*4, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ngf*4),

            nn.Conv2d(ngf*4, ngf, 1, 1),
            nn.Dropout2d(inplace=True),
            nn.BatchNorm2d(ngf),
            nn.ReLU(True),


            nn.Conv2d(ngf, ngf, 3, 1, 1),
            nn.Dropout2d(inplace=True),
            nn.BatchNorm2d(ngf),
            nn.ReLU(True),

            nn.Conv2d(ngf, ngf*4, 3, 1, 1),
            nn.Dropout2d(inplace=True),
            nn.BatchNorm2d(ngf*4),
            nn.ReLU(True),


            nn.ConvTranspose2d(ngf*4, ngf*2, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ngf*2),
            nn.ReLU(True),


            nn.ConvTranspose2d(ngf*2, ngf, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ngf),
            nn.ReLU(True),

            nn.ConvTranspose2d(ngf, nc, 4, 2, 1, bias=False),
            nn.Sigmoid()
        )

        self.encode_text = nn.Sequential(
            nn.Linear(nte, nt), nn.LeakyReLU(0.2, inplace=True))

    def forward(self, inp, txt_embed):
        encoded_txt = self.encode_text(txt_embed).view(-1, self.nt, 1, 1)
        output = self.main(torch.cat((inp, encoded_txt), 1))
        return output


class Discriminator(nn.Module):
    """This class defines a discriminator network"""

    def __init__(self, nc, ndf, nte, nt):
        super(Discriminator, self).__init__()
        self.nc = nc
        self.nt = nt
        self.nte = nte
        self.main = nn.Sequential(
            nn.Conv2d(nc, ndf, 4, 2, 1, bias=False),
            nn.LeakyReLU(0.2, inplace=True),

            nn.Conv2d(ndf, ndf*2, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ndf*2),
            nn.LeakyReLU(0.2, inplace=True),

            nn.Conv2d(ndf*2, ndf*4, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ndf*4),
            nn.LeakyReLU(0.2, inplace=True),

            nn.Conv2d(ndf*4, ndf*8, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ndf*8),

            nn.Conv2d(ndf*8, ndf*2, 1, 1),
            nn.BatchNorm2d(ndf*2),
            nn.LeakyReLU(0.2, inplace=True),

            nn.Conv2d(ndf*2, ndf*2, 3, 1, 1),
            nn.BatchNorm2d(ndf*2),
            nn.LeakyReLU(0.2, inplace=True),

            nn.Conv2d(ndf*2, ndf*8, 3, 1, 1),
            nn.BatchNorm2d(ndf*8),
            nn.LeakyReLU(0.2, inplace=True)
        )

        self.encode_text = nn.Sequential(
            nn.Linear(nte, nt),
            nn.LeakyReLU(0.2, inplace=True)
        )

        self.concat_image_n_text = nn.Sequential(
            nn.Conv2d(ndf*8+nt, ndf*8, 1, 1, 0, bias=False),
            nn.BatchNorm2d(ndf*8),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Conv2d(ndf*8, 1, 4, 1, 0, bias=False),
            nn.Sigmoid()
        )

    def forward(self, inp, txt_embed):
        encoded_img = self.main(inp)
        encoded_text = self.encode_text(txt_embed).view(-1, self.nt, 1, 1)
        encoded_text = encoded_text.repeat(1, 1, 4, 4)
        output = self.concat_image_n_text(torch.cat((encoded_img,
                                                     encoded_text), 1))

        return output.view(-1, 1).squeeze(1), encoded_img
