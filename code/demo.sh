TEXT_ENCODER= lm_sje_cub_c10_hybrid_0.00070_1_10_trainvalids.txt.t7\
CAPTION_PATH=test_captions\
GPU=0 \

export CUDA_VISIBLE_DEVICES=${GPU}

net_txt=models/text_encoder/${TEXT_ENCODER} \
queries=${CAPTION_PATH}.txt \
filenames=${CAPTION_PATH}.t7 \
gpu=${GPU} \
th demo/get_embedding.lua

#
# Generate image from text embeddings
#
python demo/demo.py \
       --caption_path ${CAPTION_PATH}.t7
