import os
import random
import logging

import torch
import torch.backends.cudnn as cudnn
from torch.utils.data import DataLoader
from dataset import TextDataset
import utils
from model import Model

logging.basicConfig(filename='txt_to_img.log', filemode='w',
                    format='%(asctime)s - %(message)s', level=logging.INFO)

opt = utils.Parse_Argument()
utils.write_config(opt, opt.cfg)
logging.info("The configurations are written to %s", opt.cfg)


#  set seed
if opt.manualSeed is None:
    opt.manualSeed = random.randint(1, 10000)
random.seed(opt.manualSeed)
torch.manual_seed(opt.manualSeed)

if opt.cuda:
    torch.cuda.manual_seed_all(opt.manualSeed)

cudnn.benchmark = True

if not opt.eval:
    logging.info("TRAIN MODE")
    if not os.path.exists(opt.outdir):
        os.makedirs(opt.outdir)

    dataset = TextDataset(opt.data_dir, split='train',
                          transform=utils.image_transform(opt.imgsize))
    logging.info("Train dataset created.")
    data_loader = DataLoader(dataset, batch_size=opt.batchsize, shuffle=True,
                             num_workers=opt.workers)
    logging.info("Dataset loaded.")
    model = Model(data_loader, opt)
    logging.info("Model defined.")
    logging.info("Start training.")
    model.train()
    logging.info("Training complete")

else:
    logging.info("TEST MODE")
    if not os.path.exists(opt.eval_dir):
        os.makedirs(opt.eval_dir)
    dataset = TextDataset(opt.data_dir, split='test',
                          transform=utils.image_transform(opt.imgsize))
    logging.info("Test dataset created.")
    data_loader = DataLoader(dataset, batch_size=opt.batch_size, shuffle=True,
                             num_workers=opt.workers)
    logging.info("Dataset loaded.")
    model = Model(data_loader, opt)
    logging.info("model defined")
    logging.info("Start testing")
    model.test()
    logging.info("Testing complete")
