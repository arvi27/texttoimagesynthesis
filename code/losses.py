import torch
import torch.nn as nn
from torch.autograd import Variable


class Loss():
    """This class calculates the losses in the network, namely,
discriminator loss, generator loss, and encoder loss"""

    def __init__(self, netD, netG, netE, real_img, text_embed,
                 inp, noise, label):
        self.netD = netD
        self.netG = netG
        self.netE = netE
        self.real_img = real_img
        self.text_embed = text_embed
        self.inp = inp
        self.noise = noise
        self.label = label
        self.batch_size = real_img.size(0)
        self.nz = noise.size(1)
        self.criterion = nn.BCELoss()
        if torch.cuda.is_available():
            self.criterion = self.criterion.cuda()

    def D_loss(self):
        """This function calculates the discriminator losses, i.e.,
loss when real image is passed, loss when output from encoded image is passed,
and loss when the image geneerated from noise is passed."""

        self.netD.zero_grad()
        # train with real images
        self.inp.resize_as_(self.real_img).copy_(self.real_img)
        self.label.resize_(self.batch_size).fill_(1)
        labelv = Variable(self.label)
        inpv = Variable(self.inp)
        output, ld_r = self.netD(inpv, self.text_embed)
        errD_real = self.criterion(output, labelv)
        errD_real.backward()
        # train with wrong images
        inpv = torch.cat((inpv[1:], inpv[:1]), 0)
        output, ld_w = self.netD(inpv, self.text_embed)
        errD_wrong = self.criterion(output, labelv)*0.5
        errD_wrong.backward()
        # train with output from encoder
        z, kld = self.netE(self.real_img)
        self.kld = kld.mean()
        z = z.reshape(self.batch_size, self.nz, 1, 1)
        self.encoded_img = self.netG(z, self.text_embed)
        self.recon_loss = self.criterion(self.encoded_img.view(self.batch_size, -1),
                                         self.real_img.view(self.batch_size, -1))
        output, ld_e = self.netD(self.encoded_img.detach(), self.text_embed)
        labelv = Variable(self.label.fill_(0))
        errD_enc = self.criterion(output, labelv)*0.5
        errD_enc.backward()
        # train with noise
        self.noise.resize_(self.batch_size, self.nz, 1, 1).normal_(0, 1)
        noisev = Variable(self.noise)
        self.fake = self.netG(noisev, self.text_embed)
        output, ld_n = self.netD(self.fake.detach(), self.text_embed)
        errD_fake = self.criterion(output, labelv)*0.5
        errD_fake.backward()
        # print(errD_real.data, errD_wrong.data, errD_enc.data, errD_fake.data)

        return errD_real, errD_wrong, errD_enc, errD_fake

    def G_loss(self):
        """This function calculates the generator losses, i.e., loss when the encoded
image is passed, and loss when the image generated from the noise is passes."""

        labelv = Variable(self.label.fill_(1))
        output, ldf = self.netD(self.fake, self.text_embed)
        errG_fake = self.criterion(output, labelv)
        errG_fake.backward()

        output, ld_eg = self.netD(self.encoded_img.detach(), self.text_embed)
        errG_enc = self.criterion(output, labelv) - 0.5 * self.recon_loss
        errG_enc.backward(retain_graph=True)

        return errG_fake, errG_enc

    def E_loss(self):
        """ This function calculates the encoder loss:
prior loss(kld) + reconstruction loss"""

        errE = self.recon_loss + self.kld
        errE.backward()
        return errE
