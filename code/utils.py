import argparse
import vae_gan
import torch
import torchvision.transforms as transforms


def Parse_Argument():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', default='./data/',
                        help='path to dataset')
    parser.add_argument('--workers', type=int, default=4,
                        help='number of data loading threads')
    parser.add_argument('--batchsize', type=int, default=64,
                        help='Size of the input minibatch')
    parser.add_argument('--imgsize', type=int, default=64,
                        help='size of the input image')
    parser.add_argument('--nte', type=int, default=1024,
                        help='size of text embedding vector')
    parser.add_argument('--nt', type=int, default=256,
                        help='compressed text embedding vector')
    parser.add_argument('--nz', type=int, default=100,
                        help='size of noise vector')
    parser.add_argument('--ngf', type=int, default=64)
    parser.add_argument('--ndf', type=int, default=64)
    parser.add_argument('--nEpochs', type=int, default=250,
                        help='Number of training epochs')
    parser.add_argument('--lr', default=0.0002, help='learning rate')
    parser.add_argument('--beta1', default=0.5, help='beta1 of adam optimizer')
    parser.add_argument('--cuda', action='store_true', help='enables cuda')
    parser.add_argument('--ngpu', type=int, default=1,
                        help='number of GPUs to use')
    parser.add_argument('--netG', default='',
                        help="path to pretrained generator")
    parser.add_argument('--netD', default='',
                        help="path to pretrained discriminator")
    parser.add_argument('--netE', default='',
                        help="path to pretrained Encoder")
    parser.add_argument('--outdir', default='./output/',
                        help='folder to output images and model checkpoints')
    parser.add_argument('--eval_dir', default='./eval_results',
                        help='folder tp save the test results')
    parser.add_argument('--manualSeed', type=int, help='manual seed')
    parser.add_argument('--eval', action='store_true',
                        help="choose whether to train the model or show demo")
    parser.add_argument('--cfg', default='./config.txt',
                         help='Path to file that stores the configurations of the model')
    opt = parser.parse_args()
    return opt


def write_config(opt, config_file):
    s = ''
    for k, v in vars(opt).items():
        s += "%s = %s\n" % (k, v)
    with open(config_file, "w") as f:
        f.write(s)


def read_config(config_file):
    cmds = {}
    with open(config_file, 'r') as cfg:
        for line in cfg:
            cmd, val = line.strip().split('=')
            cmds[cmd] = val.strip()
    return cmds


def Initialize_networks(nz, nc, nte, nt, ngf, ndf,
                        netD_path, netG_path, netE_path):
    netG = vae_gan.Generator(nz, ngf, nc, nte, nt)
    netG.apply(init_weights)
    if netG_path != '':
        netG.load_state_dict(torch.load(netG_path))
    netD = vae_gan.Discriminator(nc, ndf, nte, nt)
    netD.apply(init_weights)
    if netD_path != '':
        netD.load_state_dict(torch.load(netD_path))
    netE = vae_gan.Encoder(nz)
    netE.apply(init_weights)
    if netE_path != '':
        netE.load_state_dict(torch.load(netE_path))

    return netG, netD, netE


def init_weights(m):
    """ This function initializes a convolutional network."""
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        m.weight.data.normal_(0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        m.weight.data.normal_(1.0, 0.02)
        m.bias.data.fill_(0)


def image_transform(imageSize):
    image_transform = transforms.Compose([
        transforms.RandomCrop(imageSize),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize((0, 0, 0), (1, 1, 1))])
    return image_transform
