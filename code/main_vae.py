import argparse
import os
import random

import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim as optim
from torch.utils.data import DataLoader
import torchvision.transforms as transforms
import torchvision.utils as vutils
from torch.autograd import Variable
import vae_gan
from dataset import TextDataset
from utils import init_weights


def Parse_Argument():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', default='./data/',
                        help='path to dataset')
    parser.add_argument('--workers', type=int, default=4,
                        help='number of data loading threads')
    parser.add_argument('--batchsize', type=int, default=64,
                        help='Size of the input minibatch')
    parser.add_argument('--imgsize', type=int, default=64,
                        help='size of the input image')
    parser.add_argument('--nte', type=int, default=1024,
                        help='size of text embedding vector')
    parser.add_argument('--nt', type=int, default=256,
                        help='compressed text embedding vector')
    parser.add_argument('--nz', type=int, default=100,
                        help='size of noise vector')
    parser.add_argument('--ngf', type=int, default=64)
    parser.add_argument('--ndf', type=int, default=64)
    parser.add_argument('--nEpochs', type=int, default=250,
                        help='Number of training epochs')
    parser.add_argument('--lr', default=0.0002, help='learning rate')
    parser.add_argument('--beta1', default=0.5, help='beta1 of adam optimizer')
    parser.add_argument('--cuda', action='store_true', help='enables cuda')
    parser.add_argument('--ngpu', type=int, default=1,
                        help='number of GPUs to use')
    parser.add_argument('--netG', default='',
                        help="path to pretrained generator")
    parser.add_argument('--netD', default='',
                        help="path to pretrained discriminator")
    parser.add_argument('--netE', default='',
                        help="path to pretrained Encoder")
    parser.add_argument('--outdir', default='./output/',
                        help='folder to output images and model checkpoints')
    parser.add_argument('--manualSeed', type=int, help='manual seed')
    parser.add_argument('--eval', action='store_true',
                        help="choose whether to train the model or show demo")
    parser.add_arguments('--cfg', default='./config.txt',
                         help=Path to file that stores the configurations of the model)
    opt = parser.parse_args()
    return opt

opt = Parse_Argument()


def write_config(config_file):
    s = ''
    for k, v in vars(opt).items():
        s += "%s = %s\n" % (k, v)
    with open(config_file, "w") as f:
        f.write(s)


write_config()

if not os.path.exists(opt.outdir):
    os.makedirs(opt.outdir)

if opt.manualSeed is None:
    opt.manualSeed = random.randint(1, 10000)
random.seed(opt.manualSeed)
torch.manual_seed(opt.manualSeed)

if opt.cuda:
    torch.cuda.manual_seed_all(opt.manualSeed)

cudnn.benchmark = True

ngpu = int(opt.ngpu)
nz = int(opt.nz)
ngf = int(opt.ngf)
ndf = int(opt.ndf)
nc = 3
nt = int(opt.nt)
nte = int(opt.nte)

image_transform = transforms.Compose([
        transforms.RandomCrop(opt.imageSize),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize((0, 0, 0), (1, 1, 1))
])

#  Define and initialize the models

def Initialize_networks():
    netG = vae_gan.Generator(nz, ngf, nc, nte, nt)
    netG.apply(init_weights)
    if opt.netG != '':
        netG.load_state_dict(torch.load(opt.netG))
    netD = vae_gan.Discriminator(nc, ndf, nte, nt)
    netD.apply(init_weights)
    if opt.netD != '':
        netD.load_state_dict(torch.load(opt.netD))
    netE = vae_gan.Encoder(nz)
    netE.apply(init_weights)
    if opt.netE != '':
        netE.load_state_dict(torch.load(opt.netE))

    return netG, netD, netE


netG, netD, netE = Initialize_networks()

criterion = nn.BCELoss()

inp = torch.FloatTensor(opt.batchsize, 3, opt.imgsize, opt.imgsize)
noise = torch.FloatTensor(opt.batchsize, nz, 1, 1)
label = torch.FloatTensor(opt.batchsize)
real_label = 1
fake_label = 0
fixed_noise = torch.FloatTensor(opt.batchSize, nz, 1, 1).normal_(0, 1)

if opt.cuda():
    netE.cuda()
    netD.cuda()
    netG.cuda()
    criterion.cuda()
    inp = inp.cuda()
    noise = noise.cuda()
    label = label.cuda()
    fixed_noise = fixed_noise.cuda()
fixed_noise = Variable(fixed_noise)

if not opt.eval:
    train_dataset = TextDataset(opt.data_dir, transform=image_transform)
    train_dataloader = DataLoader(train_dataset, batch_size=opt.batchsize,
                                  shuffle=True, num_workers=opt.workers)

    optimizerD = optim.Adam(netD.parameters(), lr=opt.lr,
                            betas=(opt.beta1, 0.999))
    optimizerG = optim.Adam(netG.parameters(), lr=opt.lr,
                            betas=(opt.beta1, 0.999))
    optimizerE = optim.Adam(netE.parameters(), lr=opt.lr,
                            betas=(opt.beta1, 0.999))

    for epoch in range(1, opt.nEpochs + 1):
        for i, data in enumerate(train_dataloader, 0):
            real_img, text_embed, _ = data
            batch_size = real_img.size(0)
            text_embed = Variable(text_embed)
            if opt.cuda:
                real_img = real_img.cuda()
                text_embed = text_embed.cuda()

            netD.zero_grad()
            inp.resize_as_(real_img).copy_(real_img)
            label.resize_(batch_size).fill_(real_label)
            inpv = Variable(inp)
            labelv = Variable(label)

            #  Train with real
            output, ld_r = netD(inpv, text_embed)
            errD_real = criterion(output, labelv)
            errD_real.backward()

            #  Train with wrong images
            inpv = torch.cat((inpv[1:], inpv[:1]), 0)
            output, ld_w = netD(inpv, text_embed)
            errD_wrong = criterion(output, labelv)*0.5
            errD_wrong.backward()

            #  Train with output from encoder
            z, kld = netE(real_img)
            kld = kld.mean()
            z = z.reshape(batch_size, nz, 1, 1)
            encoded_img = netG(z, text_embed)
            recon_loss = criterion(encoded_img.view(batch_size, -1),
                                   real_img.view(batch_size, -1))
            output, ld_e = netD(encoded_img.detach(), text_embed)
            labelv = Variable(label.fill_(fake_label))
            errD_enc = criterion(output, labelv)*0.5
            errD_enc.backward()

            # Train with noise
            noise.resize_(batch_size, nz, 1, 1).normal_(0, 1)
            noisev = Variable(noise)
            fake = netG(noisev, text_embed)
            output, ld_n = netD(fake.detach(), text_embed)
            errD_fake = criterion(output, labelv)*0.5
            errD_fake.backward()

            errD = errD_real + errD_wrong + errD_enc + errD_fake
            optimizerD.step()

            #  TRAIN THE GENERATOR
            netG.zero_grad()
            labelv = Variable(label.fill_(real_label))
            output, ld_f = netD(fake, text_embed)
            errG_fake = criterion(output, labelv)
            errG_fake.backward()

            output, ld_eg = netD(encoded_img.detach(), text_embed)
            errG_enc = criterion(output, labelv) - 0.5 * recon_loss
            errG_enc.backward(retain_graph=True)

            errG = errG_fake + errG_enc
            optimizerG.step()

            # TRAIN ENCODER
            errE = recon_loss + kld
            errE.backward()
            optimizerE.step()

            print('[%d/%d][%d/%d] Loss_D: %.4f Loss_G: %.4f Loss_E: %.4f'
                  % (epoch, opt.nEpochs, i, len(train_dataloader),
                     errD.data, errG.data, errE.data))

            if i % 100 == 0:
                vutils.save_image(real_img, '%s/real_samples.png' % opt.outdir,
                                  normalize=True)
                fixed_noise = Variable(fixed_noise)
                fake = netG(fixed_noise, text_embed)
                vutils.save_image(fake.data,
                                  '%s/fake_samples_epoch_%03d.png'
                                  % opt.outdir,
                                  normalize=True)
                torch.save(netG.state_dict(), '%s/netG_epoch_%d.pth'
                           % (opt.outdir, epoch))
                torch.save(netD.state_dict(), '%s/netD_epoch_%d.pth'
                           % (opt.outdir, epoch))
                torch.save(netE.state_dict(), '%s/netE_epoch_%d.pth'
                           % (opt.outdir, epoch))


else:
    test_dataset = TextDataset(opt.data_dir, transform=image_transform,
                               split='test')

    test_dataloader = DataLoader(test_dataset, batch_size=opt.batchsize,
                                 shuffle=True, num_workers=opt.workers)

    for i, data in enumerate(test_dataloader):
        real_img, text_embed, caption = data
        batch_size = real_img.size(0)
        text_embed = Variable(text_embed)

        if opt.cuda():
            real_img = real_img.cuda()
            text_embed = text_embed.cuda()
        inp.resize_as_(real_img).copy_(real_img)
        inpv = Variable(inp)

        noise.resize_(batch_size, nz, 1, 1).normal_(0, 1)
        noisev = Variable(noise)
        gen_img = netG(noisev, text_embed).detach()

        for i in range(gen_img.size()[0]):
            cap = caption[i].strip(".")
            cap = cap.replace("/", "or")
            cap = cap.replace(" ", "_")

            file_path = './eval_results/'+cap
            vutils.save_image(gen_img[i].data, file_path+'.jpg')
